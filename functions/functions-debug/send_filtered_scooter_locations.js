const admin = require('firebase-admin');
const _ = require('lodash');

module.exports = function(req, res) {
  console.log('Outside', req.body);
  admin.database().ref('/scooters/')
  .once('value', (snapshot) => {
    const liveScooters = snapshot.val();
    var scootersOnField = [];
    _.forEach(liveScooters, (childSnapshot) => {
        scootersOnField.push(childSnapshot);
    });
    console.log('Inside', req.body);
    const scootersInRadius = calculateGeoFenseAndFilterScooters(req.body,10000,1, liveScooters);
    return res.status(200).send({ 'scootersInUserRadius': scootersInRadius });
  })

}

 function calculateGeoFenseAndFilterScooters({ lat, lng }, noOfPoints, distanceInKm , liveScooters) {
  var userFence = [];
  var scootersInsideRadius = [];
  let scooterCounterInside = 0;
  let scooterCounterOutside = 0;

  //Generate 1000 points around user
  for (var i=0; i<noOfPoints; i++) {
    userFence.push(generateFencingPointsAroundUser({lat, lng}, distanceInKm));
  }

  //Get maxima and minima of lat longs around user boundary
  const latMax = _.maxBy(userFence, 'lat').lat;
  const latMin = _.minBy(userFence, 'lat').lat;
  const lngMax = _.maxBy(userFence, 'lng').lng;
  const lngMin = _.minBy(userFence, 'lng').lng

  console.log('Max value of latitude: ', _.maxBy(userFence, 'lat'));
  console.log('Min value of latitude: ', _.minBy(userFence, 'lat'));
  console.log('Max value of Longitude: ', _.maxBy(userFence, 'lng'));
  console.log('Min value of Longitude: ', _.minBy(userFence, 'lng'));

  _.forEach(liveScooters, (randomScooterPoint) => {
    if((randomScooterPoint.lat<=latMax && randomScooterPoint.lat>=latMin) && (randomScooterPoint.lng<=lngMax && randomScooterPoint.lng>=lngMin)) {
      console.log('Point is inside 1km radius');
      scooterCounterInside = scooterCounterInside + 1;
      scootersInsideRadius.push(randomScooterPoint);
    } else {
      scooterCounterOutside = scooterCounterOutside + 1;
    }
  })

  console.log('Points inside 1km radius:', scooterCounterInside);
  console.log('Points outside 1km radius:', scooterCounterOutside);

  return scootersInsideRadius;
}

function generateFencingPointsAroundUser(center, maxDist) {
  //Defining geographic constants
  const PI = 3.1415926533;
  const EARTH_RADIUS = 6372.796924;

  //Convert Coordinates to Radians
  var startLat = center.lat * (PI/180);
  var startLon = center.lng * (PI/180);

  //Rand1 and Rand2 are two random numbers with values always between 0 & 1
  var rand1 = Math.random();
  var rand2 = Math.random();

  //Convert maxDist to Radians
  maxDist = maxDist/EARTH_RADIUS;

  /*Compute a random distance from 0 to maxdist scaled so that points on
  //larger circles have a greater probability of being chosen than points on smaller
  circles as described earlier.*/
  const randomDist = maxDist;

  /*Compute a random bearing from 0 to 2*PI radians (0 to 360 degrees),
  with all bearings having an equal probability of being chosen.*/
  const bearings = 2*PI*rand2;

  /*Use the starting point, random distance and random bearing to
  calculate the coordinates of the final random point.*/
  const resultLat = Math.asin(Math.sin(startLat)*Math.cos(randomDist) + Math.cos(startLat)*Math.sin(randomDist)*Math.cos(bearings));
  let resultLon = startLon + Math.atan2(Math.sin(bearings)*Math.sin(randomDist)*Math.cos(startLat), Math.cos(randomDist) - Math.sin(startLat)*Math.sin(resultLat));

  if(resultLon<-PI) {
    resultLon = resultLon + 2*PI;
  }

  if(resultLon>PI) {
    resultLon = resultLon - 2*PI;
  }

  return { 'lat': resultLat*(180/PI), 'lng': resultLon*(180/PI), 'bearing': bearings*(180/PI) };
}
