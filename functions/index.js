const functions = require('firebase-functions');
const sendUserRadiusFilteredScooterLocations = require('./send_filtered_scooter_locations');
const admin = require('firebase-admin');
const serviceAccount = require('./service_account.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://bbeepscooter.firebaseio.com"
});

exports.sendUserRadiusFilteredScooterLocations = functions.https.onRequest(sendUserRadiusFilteredScooterLocations);
